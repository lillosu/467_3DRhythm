# desc: this script converts TapStudio json data to OSU CS467 F2020 Rhythm Game Team 'NoteStep" json data
#       this script does not handle TapStudio "hold" data
# cmd execution: python3 TapStudioData2NoteSteps.py in_file out_file

import argparse
import json

""" main """

parser = argparse.ArgumentParser()
parser.add_argument('in_file', type=str, nargs=1)
parser.add_argument('out_file', type=str, nargs=1)
args = parser.parse_args()

with open(args.in_file[0]) as f:
    tap_studio_json = json.load(f)
    note_step_json = {"noteSteps": []}

    # init the starting last tap as the first tap in the data entry
    last_tap = tap_studio_json["songTaps"][0]
    rail_numbers = [tap_studio_json["songTaps"][0]['column']]

    for current_tap in tap_studio_json["songTaps"][1:]:
        if current_tap["tapTime"] != last_tap["tapTime"]:
            note_step_json['noteSteps'].append({"tapTime": last_tap["tapTime"], "railNumbers": rail_numbers})
            rail_numbers = []

        rail_numbers.append(current_tap["column"])
        last_tap = current_tap

    # handle last tap
    note_step_json['noteSteps'].append({"tapTime": current_tap["tapTime"], "railNumbers": rail_numbers})

with open(args.out_file[0], 'w') as note_step_file:
    json.dump(note_step_json, note_step_file, indent=4)