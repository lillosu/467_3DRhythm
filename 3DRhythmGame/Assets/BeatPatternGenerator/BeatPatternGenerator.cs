﻿// Write NotePatterns from audio file analysis using adaptions of various onset detection algorithms
// Uses NLayer library for song sampling and FFT. See: https://github.com/naudio/NLayer
// References:
// https://www.researchgate.net/publication/2563893_A_Hybrid_Approach_To_Musical_Note_Onset_Detection
// http://www.ams.org/notices/200903/rtx090300356p.pdf
// https://ccrma.stanford.edu/~jos/dft/
// https://www.badlogicgames.com/wordpress/?m=201002&paged=6

using System;
using System.Collections.Generic;
using System.Linq;
using NLayer;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using UnityEngine;

namespace BeatPatternGenerator
{
  class BeatPatternGenerator
  {
    const int SAMPLE_WINDOW = 2048; // Samples per window. Remember Stereo takes up two samples per hit (so twice the size for same amount of time as Mono sampling).
    const int SAMPLE_RATE = 44100; // Hz

    // Adjust these for note pattern/difficulty changes
    const int THRESHOLD_SIZE = 35; // Range of previous/following flux values to average for threshold estimation. Can be adjusted depending on results. Higher generally means more beats.
    float THRESHOLD_MULTIPLIER = 1.5f; // Multiplier for threshold so 'slightly-above-average' values are dropped. Can be changed. 1.5-2.0 anecdotally seems pretty good. Higher means less beats pass the threshold.
    float BEAT_BUFFER = 8; // Samples (#indicies, so time dependent on INDEX_TO_TIME_FACTOR) to delay after onset selection. Higher means larger minimal delay between notes.
    int HELD_NOTE_THRESHOLD = 5; // Number of sequential notes required to generate a held note.

    const float INDEX_TO_TIME_FACTOR = (float)SAMPLE_WINDOW / (float)SAMPLE_RATE / 2f; // Conversion factor of a flux index to time position of an audio file.

    public BeatPatternGenerator(String path, bool difficult = false) {
      // TODO: Check for valid path. Likely user won't be dictating it and driven by our program, but could run into permissions quirks on mobile
      audioName = Path.GetFileNameWithoutExtension(path);
      notePatternName = audioName + "_NotePattern";
      if (difficult) {
        notePatternName += "D";
        THRESHOLD_MULTIPLIER = 1.4f;
        BEAT_BUFFER = 6;
        HELD_NOTE_THRESHOLD = 7;
      }
      //CreateBuiltInNotePatterns();
      audioClip = SongList.audioClip;
      difficulty = difficult;
    }

    AudioClip audioClip;
    int readPos = 0;
    bool difficulty = false;
    public String audioName { get; set; }
    public String notePatternName { get; set; }
    // (time, rail)
    List<(List<float>, int)> beats { get; set; }

    // Process audioFile for onsets then write beats to a NotePattern
    public void Execute() {
      List<float> flux = CalculateFlux();
      List<float> normalizedFlux = Normalize(FilterFlux(flux, CalculateThreshold(flux)));
      CalculatePeaks(normalizedFlux);
      BinNotes(normalizedFlux);
      BeatsToJSON();
    }

    private void BeatsToJSON() {
      StringBuilder sb = new StringBuilder();
      StringWriter sw = new StringWriter(sb);
#if UNITY_EDITOR
      using (StreamWriter file = File.CreateText(Path.Combine(Application.dataPath, "Resources", "NoteData", notePatternName + ".json"))) {
#else
      using (StreamWriter file = File.CreateText(Path.Combine(Application.persistentDataPath, notePatternName + ".json"))) {
#endif
        using (JsonWriter writer = new JsonTextWriter(sw)) {
          writer.Formatting = Formatting.Indented;
        
          writer.WriteStartObject();
          writer.WritePropertyName("noteSteps");
          writer.WriteStartArray();
          foreach ((List<float> tapTimes, int rail) beat in beats) {
            writer.WriteStartObject();
            writer.WritePropertyName("tapTimeFrame");
            writer.WriteStartArray();
            foreach (float tapTime in beat.tapTimes) {
              writer.WriteValue(tapTime);
            }
            writer.WriteEndArray();
            writer.WritePropertyName("railNumbers");
            writer.WriteStartArray();
            writer.WriteValue(beat.rail);
            writer.WriteEndArray();
            writer.WriteEndObject();
          }
          writer.WriteEndArray();
          writer.WriteEndObject();
          writer.Close();
          JsonSerializer serializer = new JsonSerializer();
          file.Write(sb.ToString());
        }
      }
    }

    // Calculates the flux of the signal's FFT, basically the difference in magnitude across windows
    private List<float> CalculateFlux() {
      List<float> flux = new List<float>();
      List<float> prevSpectrum = new List<float>();

      // Double samples because of stereo channel, iterate by half of the sample window for the same reason.
      for (readPos = 0; readPos < (audioClip.samples * 2); readPos += SAMPLE_WINDOW / 2) {
        List<float> spectrum = ProcessFFT(AverageStereo(PrepareSamples()));

        if (!flux.Any())
          prevSpectrum = new List<float>(spectrum.Count);

        flux.Add(CalculatePositiveFlux(prevSpectrum, spectrum));

        prevSpectrum = spectrum;
      }
      flux[0] = flux[flux.Count - 1] = 0; // Remove overpowering noisy starting/ending values. I'm not sure what to do with these otherwise.

      return flux;
    }

    // Filter flux by the threshold values generated by CalculateThreshold
    private List<float> FilterFlux(List<float> flux, List<float> thresholds) {
      List<float> filteredFlux = new List<float>();

      foreach (var fluxPair in flux.Zip(thresholds, Tuple.Create)) {
        if (fluxPair.Item1 > fluxPair.Item2)
          filteredFlux.Add(fluxPair.Item1 - fluxPair.Item2);
        else
          filteredFlux.Add(0f);
      }
        
      return filteredFlux;
    }

    private List<float> Normalize(List<float> values) {
      List<float> normalizedValues = new List<float>();
      float min = values.Min();
      float max = values.Max();
      normalizedValues = values.Select( i => (i - min) / (max - min)).ToList();

      return normalizedValues;
    }

    // Average left/right audio channels in a stereo music file
    private float[] AverageStereo(float[] samples)
    {
      float[] sampleAvg = new float[samples.Length / 2];
      int avgIndex = 0;

      for(int i = 0; i < samples.Length; i+=2 ) {
        sampleAvg[avgIndex] = (samples[i] + samples[i+1]) / 2;
        ++avgIndex;
      }

      return sampleAvg;
    }

    // Read samples from audio file and average the stereo. audioFile position will update after reading.
    public float[] PrepareSamples() {
      float[] samples = new float[SAMPLE_WINDOW];
      try {
        audioClip.GetData(samples, readPos);
      } catch {
        // If it breaks for some reason, it's probably near the end reads of the song. Just continue with what we've got instead of blowing up.
      }

      return samples;
    }

    private List<float> ProcessFFT(float[] samples) {
      // Get largest power of 2 to process FFT. May change if we use different sampling windows.
      int fftPoints = 2;
      while (fftPoints * 2 <= samples.Length)
        fftPoints *= 2;

      NAudio.Dsp.Complex[] fftFull = new NAudio.Dsp.Complex[fftPoints];

      // Apply smoothing function (Hamming window) to points before FFT. Makes the result less noisy.
      for (int i = 0; i < fftPoints; ++i) {
        fftFull[i].X = (float)(samples[i] * NAudio.Dsp.FastFourierTransform.HammingWindow(i, fftPoints));
      }

      NAudio.Dsp.FastFourierTransform.FFT(true, (int)Math.Log(fftPoints, 2), fftFull);
      List<float> dataSpectrum = new List<float>();

      // FFT is mirrored, drop redundant data (fftPoints / 2). Can combine them, but dropping is fine for our purposes. We want the PSD (magnitude) of the FFT.
      // Bin width is sample rate divided by fft length. First bin is half that (from mirroring) : https://www.ap.com/technical-library/more-about-ffts/
      for (int i = 0; i < fftPoints / 2; ++i)
        dataSpectrum.Add(ComplexToMagnitude(fftFull[i]));

      return dataSpectrum;
    }

    // Calculate magnitude (Power Spectral Density) of FFT. Combines real and imaginary parts.
    private float ComplexToMagnitude(NAudio.Dsp.Complex fftSample) {
      return (float)Math.Sqrt(Math.Pow(fftSample.X, 2) + Math.Pow(fftSample.Y, 2));
    }

    // Zeroes negative flux values, they are meaningless for our purposes in onset beat detection.
    private float CalculatePositiveFlux(List<float> prevSpectrum, List<float> spectrum) {
      float flux = 0;

      foreach (var spectra in prevSpectrum.Zip(spectrum, Tuple.Create)) {
        float val = spectra.Item2 - spectra.Item1;
        flux += val > 0 ? val : 0;
      }

      return flux;
    }

    // Calculate threshold to filter flux noise. This is the metric we will use to drop data below the threshold.
    // THRESHOLD_MULTIPLIER and THRESHOLD_SIZE are arbitrary and can be adjusted. See vars for typical ranges.
    private List<float> CalculateThreshold(List<float> flux) {
      List<float> thresholds = new List<float>();

      for (int i = 0; i < flux.Count; ++i) {
        // Get flux indices before and after current flux
        int start = Math.Max(0, i - THRESHOLD_SIZE);
        int end = Math.Min(flux.Count - 1, i + THRESHOLD_SIZE);

        // Average flux window for threshold value
        float mean = 0;
        for (int j = start; j <= end; ++j)
          mean += flux[j];

        mean /= (end - start);
        thresholds.Add(mean * THRESHOLD_MULTIPLIER);
      }

      return thresholds;
    }

    // Select onset peaks from normalized flux values
    private void CalculatePeaks(List<float> normalizedFlux) {
      // Strip peak follow-ups
      for (int i = 0; i < normalizedFlux.Count - 1 - BEAT_BUFFER; ++i) {
        if (normalizedFlux[i] > normalizedFlux[i+1]) {
          normalizedFlux[i+1] = 0;

          // Further strip beats by BEAT_BUFFER
          for (int j = i + 1; j < i + BEAT_BUFFER; ++j) {
            if (normalizedFlux[j] > 0)
              normalizedFlux[j] = 0;
          }

          // Hacky, but strip leading beats. TODO: Move to separate lane / hold pattern if notes are too close
          if (i > 3) {
            normalizedFlux[i-1] = 0;
            normalizedFlux[i-2] = 0;
            normalizedFlux[i-3] = 0;
            normalizedFlux[i-4] = 0;
          }
        }
      }
    }

    // Splits peaks into bins for lane distribution
    private void BinNotes(List<float> peaks) {
      beats = new List<(List<float>, int)>();
      List<float> storedNotes = new List<float>();
      int streakCount = 0;
      int streakRail = 0;

      foreach (var (peak, index) in peaks.Select((peak, index) => (peak, index))) {
        if (peak == 0 || index * INDEX_TO_TIME_FACTOR < 1)
          continue;

        List<float> noteTimes = new List<float>();
        float noteTime = index * INDEX_TO_TIME_FACTOR;

        int rail = 0;

        if (difficulty) {
          if (peak >= 0.33) {
            rail = 0;
          } else if (peak < 0.33 && peak >= 0.14) {
            rail = 1;
          } else if (peak < 0.14 && peak >= 0.06 ) {
            rail = 2;
          } else {
            rail = 3;
          }
        } else {
          if (peak >= 0.30) {
            rail = 0;
          } else if (peak < 0.30 && peak >= 0.12) {
            rail = 1;
          } else {
            rail = 2;
          }
        }

        // Keep track of successive notes
        if (rail == streakRail) {
          ++streakCount;
        }
        else { // Note is on new rail, add all stored notes
          foreach (float note in storedNotes) {
            if (streakCount < HELD_NOTE_THRESHOLD) { // Not long enough for held note, add individual notes
              List<float> singleNote = new List<float>() {note};
              beats.Add((singleNote, streakRail));
            } else {
              noteTimes.Add(note);
            }
          }

          if (noteTimes.Any()) // Any notes in noteTimes will become held note
            beats.Add((noteTimes, streakRail));

          // Reset stored notes, count, change tracked rail
          storedNotes.Clear();
          streakCount = 0;
          streakRail = rail;
        }

        storedNotes.Add(noteTime);
      }
    }


// Editor use only, helpers for built-in note patterns and hold note demo.

    public void CreateBuiltInNotePatterns() {
      List<string> songList = new List<string>();
      var audioClips = Resources.LoadAll("Audio", typeof(AudioClip));
      foreach (AudioClip clip in audioClips) {
        audioClip = clip;
        audioName = audioClip.name;
        notePatternName = audioName + "_NotePattern";
        difficulty = false;
        THRESHOLD_MULTIPLIER = 1.5f;
        BEAT_BUFFER = 8;
        HELD_NOTE_THRESHOLD = 5;

        Execute();

        notePatternName += "D";
        THRESHOLD_MULTIPLIER = 1.4f;
        BEAT_BUFFER = 5;
        HELD_NOTE_THRESHOLD = 7;
        difficulty = true;

        Execute();
      }
    }

    public void GenerateHoldNotes() {
      List<float> noteTimes = new List<float>();
      List<(List<float>, int)> bs = new List<(List<float>, int)>();

      for (int i = 0; i < 3000; i+=50) {
        List<float> beatList = new List<float>();
        for (int j = i; j < i+46; ++j) {
          float noteTime = j * INDEX_TO_TIME_FACTOR;
          beatList.Add(noteTime);
        }
        bs.Add((beatList, 1));
      }

      StringBuilder sb = new StringBuilder();
      StringWriter sw = new StringWriter(sb);
#if UNITY_EDITOR
      using (StreamWriter file = File.CreateText(Path.Combine(Application.dataPath, "Resources", "NoteData", "holdNotes" + ".json")))
      {
#else
      using (StreamWriter file = File.CreateText(Path.Combine(Application.persistentDataPath, "holdNotes" + ".json"))) {
#endif
        using (JsonWriter writer = new JsonTextWriter(sw))
        {
          writer.Formatting = Formatting.Indented;

          writer.WriteStartObject();
          writer.WritePropertyName("noteSteps");
          writer.WriteStartArray();
          foreach ((List<float> tapTimes, int rail) beat in bs)
          {
            writer.WriteStartObject();
            writer.WritePropertyName("tapTimeFrame");
            writer.WriteStartArray();
            foreach (float tapTime in beat.tapTimes)
            {
              writer.WriteValue(tapTime);
            }
            writer.WriteEndArray();
            writer.WritePropertyName("railNumbers");
            writer.WriteStartArray();
            writer.WriteValue(beat.rail);
            writer.WriteEndArray();
            writer.WriteEndObject();
          }
          writer.WriteEndArray();
          writer.WriteEndObject();
          writer.Close();
          JsonSerializer serializer = new JsonSerializer();
          file.Write(sb.ToString());
        }
      }
    }
  }
}
