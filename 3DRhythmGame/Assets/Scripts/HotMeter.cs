﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HotMeter : MonoBehaviour
{
    public Slider slider;
    public Transform burst_mult;
    // Start is called before the first frame update
    void Start()
    {
        slider.value = 0;
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = Notes_Spawn.hotMeter;
    }
    
}
