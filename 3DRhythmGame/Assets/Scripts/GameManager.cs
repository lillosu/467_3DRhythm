﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Newtonsoft.Json.Linq;
using PathCreation;
using TMPro;
using UnityEngine;

using BeatPatternGenerator;


public class GameManager : MonoBehaviour
{
    [SerializeField] float delayDuration;
    [SerializeField] Transform dataManager;
    [SerializeField] GameObject gameOverScreen;
    [SerializeField] GameObject finalScoreDisplay;
    [SerializeField] GameObject finalAccDisplay;
    [SerializeField] GameObject currentScore;
    [SerializeField] GameObject currentHotStreak;
    [SerializeField] GameObject currentNoteStreak;
    [SerializeField] GameObject currentMissedStreak;
    [SerializeField] public PathCreator rail0;
    [SerializeField] public PathCreator rail1;
    [SerializeField] public PathCreator rail2;
    [SerializeField] public PathCreator rail3;
    [SerializeField] public Transform railZeroNoteObj;
    [SerializeField] public Transform railOneNoteObj;
    [SerializeField] public Transform railTwoNoteObj;
    [SerializeField] public Transform railThreeNoteObj;
    [SerializeField] public Transform holdTailNoteObj;
    [SerializeField] public float tailNoteSpacing;
    [SerializeField] public GameObject tapper0;
    [SerializeField] public GameObject tapper1;
    [SerializeField] public GameObject tapper2;
    [SerializeField] public GameObject tapper3;
    [SerializeField] public GameObject inaccText;
    [SerializeField] public GameObject missText;
    public static bool gameOver = false;
    public NoteStepList noteSteps;
    string jsonFile;
    public static int noteCount;


    void Awake()
    {
        // This should only be null if the GameScene itself was launched or Select Device Song is empty. It should be set by SelectASong otherwise.
        if (LevelData.SongName is null) {
            LevelData.SongName = "Chances - Silent Partner";
            LevelData.userSong = false;
        }

        if (!LevelData.userSong)
        {
            SongList.audioClip = Resources.Load<AudioClip>(Path.Combine("Audio", LevelData.SongName));
        }
        else
        {
            SongList.GetUserAudioClip(LevelData.SongName);
        }

        BeatPatternGenerator.BeatPatternGenerator bpg = new BeatPatternGenerator.BeatPatternGenerator(LevelData.SongName, LevelData.hardMode);

        // Generate note patterns only if they do not exist already
#if UNITY_EDITOR
        if (!File.Exists(Path.Combine(Application.dataPath, "Resources", "NoteData", bpg.notePatternName + ".json")))
            bpg.Execute();
#else
      if (LevelData.userSong) {
        if (!File.Exists(Path.Combine(Application.persistentDataPath, bpg.notePatternName + ".json")))
          bpg.Execute();
      }
#endif

        LevelData.NoteJson = bpg.notePatternName;

        LoadJson();

        Debug.Log(jsonFile);

        noteCount = 0;
        noteSteps = new NoteStepList();
        var jo = JObject.Parse(jsonFile);
        NoteStep noteStep = new NoteStep();
        for (int i = 0; i < ((JArray)jo["noteSteps"]).Count; i++)
        {
            noteStep = jo["noteSteps"][i].ToObject<NoteStep>();
            noteSteps.noteSteps.Add(noteStep);
            noteCount += noteStep.railNumbers.Count();
        }
    }

    void LoadJson()
    {
        Debug.Log("path: " + Path.Combine(Application.dataPath, "Resources", "NoteData", LevelData.NoteJson + ".json"));
#if UNITY_EDITOR
        jsonFile = File.ReadAllText(Path.Combine(Application.dataPath, "Resources", "NoteData", LevelData.NoteJson + ".json"));
#else
      if (!LevelData.userSong)
        jsonFile = Resources.Load<TextAsset>(Path.Combine("NoteData", LevelData.NoteJson)).text;
      else
        jsonFile = File.ReadAllText(Path.Combine(Application.persistentDataPath, LevelData.NoteJson + ".json"));
#endif
    }

    // Start is called before the first frame update
    void Start()
    {

      StartCoroutine(StartSong());

    }

    IEnumerator StartSong()
    {
        Debug.Log("in coroutine");

        // wait a few seconds for the poor performance immediately after level load to subside
        yield  return new WaitForSeconds(delayDuration);
        Debug.Log("delay done");

        // set the data manager's parent to the game manager so that in the data manager we can access the game manager's audio src and rails
        var newDataManager = Instantiate(dataManager, new Vector3(0f, 3, -10f), dataManager.rotation);
        newDataManager.parent = gameObject.transform;
        // Reset scores so we're not accumulating across gameplay loops
        Notes_Spawn.totalScore = 0;
        Notes_Spawn.hotStreak = 1;
        Debug.Log("data manager created");

    }
    void Update()
    {
        currentScore.GetComponent<TextMeshProUGUI>().text = Notes_Spawn.totalScore.ToString();
        currentHotStreak.GetComponent<TextMeshProUGUI>().text = Notes_Spawn.hotStreak.ToString();
        currentNoteStreak.GetComponent<TextMeshProUGUI>().text = Notes_Spawn.noteStreak.ToString();
        currentMissedStreak.GetComponent<TextMeshProUGUI>().text = Notes_Spawn.missTotal.ToString();

        //Of accuracy too low, then game over
        if(Notes_Spawn.accuracy <= .50 && Time.timeSinceLevelLoad > delayDuration + 60 && !gameOver && PauseMenu.PausedGame == false) {
            
            PauseMenu.PausedGame = true;
            gameOver = true;
            gameOverScreen.SetActive(true);
            missText.SetActive(false);
            finalScoreDisplay.GetComponent<TextMeshProUGUI>().text = Notes_Spawn.totalScore.ToString();
            float finalAcc = UnityEngine.Mathf.Round(Notes_Spawn.accuracy*100);
            finalAccDisplay.GetComponent<TextMeshProUGUI>().text = finalAcc.ToString()+"%";
        }
        
        //If too many misses occur, then game over
        if (MissMeter.overMiss && !gameOver && PauseMenu.PausedGame == false)
        {

            PauseMenu.PausedGame = true;
            gameOver = true;
            gameOverScreen.SetActive(true);
            inaccText.SetActive(false);
            finalScoreDisplay.GetComponent<TextMeshProUGUI>().text = Notes_Spawn.totalScore.ToString();
            float finalAcc = UnityEngine.Mathf.Round(Notes_Spawn.accuracy*100);
            finalAccDisplay.GetComponent<TextMeshProUGUI>().text = finalAcc.ToString()+"%";
        }
        
        //If the song is over, then game over
        if (Time.timeSinceLevelLoad > delayDuration + 60 && !gameOver && !GetComponent<AudioSource>().isPlaying && PauseMenu.PausedGame == false)
        {
            PauseMenu.PausedGame = true;
            gameOver = true;
            gameOverScreen.SetActive(true);
            inaccText.SetActive(false);
            missText.SetActive(false);
            finalScoreDisplay.GetComponent<TextMeshProUGUI>().text = Notes_Spawn.totalScore.ToString();
            float finalAcc = UnityEngine.Mathf.Round(Notes_Spawn.accuracy * 100);
            finalAccDisplay.GetComponent<TextMeshProUGUI>().text = finalAcc.ToString() + "%";
        }
    }

}
