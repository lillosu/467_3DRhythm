﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMainMenuAudioSource : MonoBehaviour
{
    // Start is called before the first frame update
    public void DestMainMenuAudioSource()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("MainMenuAudioSource");
        foreach (GameObject obj in objs)
        {
            Destroy(obj);
        }
    }
}
