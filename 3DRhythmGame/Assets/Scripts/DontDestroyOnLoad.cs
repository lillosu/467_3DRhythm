﻿// note: code for avoiding duplicates taken from zaid87 respones in this thread: https://answers.unity.com/questions/982403/how-to-not-duplicate-game-objects-on-dontdestroyon.html

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{
    private static DontDestroyOnLoad dontDestroyMe;

    void Awake()
    {
        DontDestroyOnLoad(this);

        if (dontDestroyMe == null)
        {
            dontDestroyMe = this;
        }

        else
        {
            Destroy(gameObject);
        }
    }
}
