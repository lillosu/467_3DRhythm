﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SongListDetail : MonoBehaviour
{
  public Text text = null;

  public void OnClick() {
    LevelData.SongName = text.text;
  }
}
