﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteMover : MonoBehaviour
{
    float unitsPerSecond = 11.0f;
    // Update is called once per frame
    void Update()
    {
        transform.Translate(unitsPerSecond * Vector3.down * Time.deltaTime);
    }
}
