﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuResetter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        PauseMenu.Unpause();
    }
}
