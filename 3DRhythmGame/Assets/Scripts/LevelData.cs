﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LevelData
{
    public static string SongName { get; set; }

    public static string NoteJson { get; set; }

    public static bool userSong { get; set; } = false;

    public static bool hardMode { get; set; }

    static LevelData()
    {
        hardMode = false;
    }
}
