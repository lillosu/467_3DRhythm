﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelectedSong : MonoBehaviour
{
  public TextMeshProUGUI selectedSong = null;

  // Start is called before the first frame update
  void Start()
    {
      selectedSong.text = "";
    }

    // Update is called once per frame
    void Update()
    {
      selectedSong.text = LevelData.SongName;
    }
}
