﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

public class NoteLaunchTimeQueue
{
    private int currentIdx;
    public List<List<float>> timeFrames;

    public NoteLaunchTimeQueue()
    {
        currentIdx = 0;
        timeFrames = new List<List<float>>();
    }

    public void addLaunchTimeFrame(List<float> timeFrame)
    {
        timeFrames.Add(timeFrame);
    }

    public List<float> getCurrentLaunchTimeFrame()
    {
        return timeFrames[currentIdx];
    }

    public void stageNext()
    {
        currentIdx++;
    }
}
