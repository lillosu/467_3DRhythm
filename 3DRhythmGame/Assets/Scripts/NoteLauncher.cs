﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteLauncher
{
    public List<NoteLaunchTimeQueue> noteQueues = new List<NoteLaunchTimeQueue>();
    public static int noteCount;
    float speed = 11.0f;
    public NoteLauncher(NoteStepList noteStepList, List<float> railLengths)
    {
        for (int i = 0; i < railLengths.Count; i++)
        {
            noteQueues.Add(new NoteLaunchTimeQueue());
        }

        for (int i = 0; i < noteStepList.noteSteps.Count; i++)
        {
            foreach (int railNumber in noteStepList.noteSteps[i].railNumbers)
            {
                List<float> offsetTimeFrame = new List<float>();
                foreach (float time in noteStepList.noteSteps[i].tapTimeFrame)
                {
                    offsetTimeFrame.Add(time - (railLengths[railNumber] / speed));
                }
                noteQueues[railNumber].addLaunchTimeFrame(offsetTimeFrame);
            }
        }
        noteCount = noteQueues.Count;
    }
}
