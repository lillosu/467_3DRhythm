﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NoteStep
{
    public List<float> tapTimeFrame;
    public int[] railNumbers;
}
