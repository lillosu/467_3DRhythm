﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Based on: https://www.youtube.com/watch?v=JivuXdrIHK0
public class PauseMenu : MonoBehaviour
{
    public static bool PausedGame;
    public GameObject pauseMenuScreen;
    // Start is called before the first frame update
    void Start()
    {
        PausedGame = false;
    }

    // Update is called once per frame
    void Update()
    {

            if (PausedGame)
            {
                Unpause();
            }
            else
            {
                Pause();
            }
        
    }
    void Pause()
    {
        //pauseMenuScreen.SetActive(true);
        Time.timeScale = 0f;
        AudioListener.pause = true;
        PausedGame = true;
    }

    public static void Unpause()
    {
        //pauseMenuScreen.SetActive(false);
        Time.timeScale = 1f;
        AudioListener.pause = false;
        PausedGame = false;
    }

    public void SwitchPaused()
    {
        if (GameManager.gameOver == false)
        {
            if (PausedGame)
            {
                Unpause();
                pauseMenuScreen.SetActive(false);
            }

            else
            {
                Pause();
                pauseMenuScreen.SetActive(true);
            }
        }
    }
}
