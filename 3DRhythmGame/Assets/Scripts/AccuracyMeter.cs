﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccuracyMeter : MonoBehaviour
{
    public Slider slider;
    public Transform burst_mult;
    public Gradient gradient;
    public Image fill;

    // Start is called before the first frame update
    void Start()
    {
        slider.value = 1f;
        fill.color = gradient.Evaluate(1f);
    }

    // Update is called once per frame
    void Update()
    {
            slider.value = calcAccuracy();
            fill.color = gradient.Evaluate(slider.value);

    }
    public float calcAccuracy()
    {
        if (Notes_Spawn.totalSucc > 1f && Notes_Spawn.totalTaps > 1f)
        {
            Notes_Spawn.accuracy = (Notes_Spawn.totalSucc - 1f) / (Notes_Spawn.totalTaps - 1f);
            return Notes_Spawn.accuracy;
        }
        else if(Notes_Spawn.totalSucc < 2f && Notes_Spawn.totalTaps > 1f)
        {
            return 0f;
        }
        else {
            return 1f;
        }
        
    }

}
