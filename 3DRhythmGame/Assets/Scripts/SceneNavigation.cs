﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.SceneManagement;

public class SceneNavigation : MonoBehaviour
{
    AudioSource forwardButtonClick;

    void Start()
    {
        forwardButtonClick = gameObject.AddComponent<AudioSource>();
        forwardButtonClick.clip = Resources.Load("GameSounds/coin_02") as AudioClip;
    }

    public void MainMenu()
    {
        StartCoroutine(playAudioClipThenLoadScene(forwardButtonClick, "MainMenu"));
    }

    public void MainMenuFromGameScene()
    {
        LoadData.scene = "MainMenu";
        SceneManager.LoadScene("Loading");
    }

    public void SelectASong()
	{
        LoadData.scene = "SelectASong";
        LevelData.userSong = false;
        LevelData.hardMode = false;

        StartCoroutine(playAudioClipThenLoadScene(forwardButtonClick, "SelectASong"));
    }

	public void SelectDeviceSong()
	{
      if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
        Permission.RequestUserPermission(Permission.ExternalStorageRead);

      if (Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead) || Application.isEditor) {
            LoadData.scene = "SelectDeviceSong";
            LevelData.userSong = true;
            LevelData.hardMode = false;

            StartCoroutine(playAudioClipThenLoadScene(forwardButtonClick, "Loading"));
      }
  }

    public void Gamescene()
    {

        if (LevelData.hardMode)
            LoadData.scene = "GameSceneHard";
        else
            LoadData.scene = "GameSceneNormal";

        // Initialize some game scene variables
        GameManager.gameOver = false;
        PauseMenu.Unpause();
        PathFollow.speed = 50;
        Notes_Spawn.noteStreak = 0;
        Notes_Spawn.hotStreak = 1;
        Notes_Spawn.totalScore = 0;
        Notes_Spawn.missStreak = 0;
        Notes_Spawn.missTotal = 0;
        Notes_Spawn.hotMeter = 0;
        Notes_Spawn.totalTaps = 1f;//track total taps
        Notes_Spawn.totalSucc = 1f;//track total successes
        Notes_Spawn.accuracy = 1f;//track total successes

        StartCoroutine(playAudioClipThenLoadScene(forwardButtonClick, "Loading"));
	  }

    public void HowToPlay()
    {
        StartCoroutine(playAudioClipThenLoadScene(forwardButtonClick, "HowToPlay"));
    }

    public void Credits()
    {
      StartCoroutine(playAudioClipThenLoadScene(forwardButtonClick, "Credits"));
    }

  IEnumerator playAudioClipThenLoadScene(AudioSource audioSource, string scene)
    {
        audioSource.Play();
        yield return new WaitForSeconds(audioSource.clip.length);
        SceneManager.LoadScene(scene);
    }
}
