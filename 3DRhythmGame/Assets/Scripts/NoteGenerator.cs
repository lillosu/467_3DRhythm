﻿/*
 citations:
    * audioSettings.dspTime as time tracking derived from this resource: 
        https://www.gamasutra.com/blogs/GrahamTattersall/20190515/342454/Coding_to_the_Beat__Under_the_Hood_of_a_Rhythm_Game_in_Unity.php
    * trick for attaining more accurate timing using a combination of audioSettings.dspTime and
        Time.unscaledDeltaTime: Nifflas's reply here:
        https://forum.unity.com/threads/do-people-not-realize-how-bad-audiosource-dsptime-is-can-someone-explain-how-it-works.402308/
*/

using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using PathCreation;

public class NoteGenerator : MonoBehaviour
{
    // todo: place these in a location more closely related to score tracking
    //Scoring Sytem variables throughout system
    public static int noteStreak = 0;//track note-streak
    public static int hotStreak = 1;//track hot-streak
    public static int totalScore = 0;//track total score

    public NoteStepList noteSteps;
    public NoteLauncher noteLauncher;

    float initialDspTime;
    float timeInSong;
    float currentTime = -1.0f;
    float lastRecordedDspDerivedTime = -1.0f;

    public List<int> currentHoldNoteParentIds;

    Transform railZeroNoteObj;
    Transform railOneNoteObj;
    Transform railTwoNoteObj;
    Transform railThreeNoteObj;
    Transform holdTailNoteObj;

    PathCreator rail0;
    PathCreator rail1;
    PathCreator rail2;
    PathCreator rail3;

    GameObject tapper0;
    GameObject tapper1;
    GameObject tapper2;
    GameObject tapper3;

    int noteId  = 0;

    // parse this song's note json file

  void Start()
    {
        noteSteps = transform.parent.GetComponent<GameManager>().noteSteps;

        currentHoldNoteParentIds = new List<int>();

        List<float> railLengths = new List<float>();

        tapper0 = GetComponentInParent<GameManager>().tapper0;
        railZeroNoteObj = GetComponentInParent<GameManager>().railZeroNoteObj;
        rail0 = GetComponentInParent<GameManager>().rail0;
        railLengths.Add(rail0.path.GetClosestDistanceAlongPath(tapper0.transform.position));
        
        tapper1 = GetComponentInParent<GameManager>().tapper1;
        railOneNoteObj = GetComponentInParent<GameManager>().railOneNoteObj;
        rail1 = GetComponentInParent<GameManager>().rail1;
        railLengths.Add(rail1.path.GetClosestDistanceAlongPath(tapper1.transform.position));
        
        tapper2 = GetComponentInParent<GameManager>().tapper2;
        railTwoNoteObj = GetComponentInParent<GameManager>().railTwoNoteObj;
        rail2 = GetComponentInParent<GameManager>().rail2;
        railLengths.Add(rail2.path.GetClosestDistanceAlongPath(tapper2.transform.position));
        
        if (rail3 = GetComponentInParent<GameManager>().rail3)
        {
            tapper3 = GetComponentInParent<GameManager>().tapper3;
            railThreeNoteObj = GetComponentInParent<GameManager>().railThreeNoteObj;
            railLengths.Add(rail3.path.GetClosestDistanceAlongPath(tapper3.transform.position));
        }

        else
        {
            tapper3 = null;
            rail3 = null;
            railThreeNoteObj = null;
        }

        holdTailNoteObj = GetComponentInParent<GameManager>().holdTailNoteObj;

        noteLauncher = new NoteLauncher(noteSteps, railLengths);

        GetComponentInParent<AudioSource>().clip = SongList.audioClip;

        initialDspTime = (float)AudioSettings.dspTime; // this needs to happen right before audio src play in the code
        GetComponentInParent<AudioSource>().Play();
    }

    void Update()
    {
        if (PauseMenu.PausedGame == false)
        {
            UpdateCurrentTime();
            // Debug.Log("current time: " + currentTime);

            LaunchNotes();
        }
    }

    private void UpdateCurrentTime()
    {
        timeInSong = (float)(AudioSettings.dspTime - initialDspTime);

        // AudioSettings.dspTime sometimes reports the same time in subsequent frames
        // if a unique value is detected, set the currentTime to that value
        if (currentTime != timeInSong && timeInSong != lastRecordedDspDerivedTime)
        {
            currentTime = timeInSong;
            lastRecordedDspDerivedTime = timeInSong;
        }

        // if a duplicate value is detected, increase currentTime by unscaledDeltaTime,
        // which is the timeScale-independent interval from the last frame to the current frame
        else
        {
            currentTime += Time.unscaledDeltaTime;
        }
    }

    private void LaunchNotes()
    {
        if (noteLauncher.noteQueues[0].timeFrames.Count > 0)
        {
            checkForLaunch(noteLauncher.noteQueues[0], railZeroNoteObj, rail0);
        }

        if (noteLauncher.noteQueues[1].timeFrames.Count > 0)
        {
            checkForLaunch(noteLauncher.noteQueues[1], railOneNoteObj, rail1);
        }

        if (noteLauncher.noteQueues[2].timeFrames.Count > 0)
        {
            checkForLaunch(noteLauncher.noteQueues[2], railTwoNoteObj, rail2);
        }
       
        if (rail3)
        {
            if (noteLauncher.noteQueues[3].timeFrames.Count > 0)
            {
                checkForLaunch(noteLauncher.noteQueues[3], railThreeNoteObj, rail3);
            }
        }
    }

    private void checkForLaunch(NoteLaunchTimeQueue notes, Transform noteObject, PathCreator rail)
    {

        // Debug.Log("launch time - currentTime: " + (notes.getCurrentLaunchTimeFrame()[0] - currentTime));

        if (Mathf.Abs(notes.getCurrentLaunchTimeFrame()[0] - currentTime) < 0.1)
        {
            Transform headNote;

            if (notes.getCurrentLaunchTimeFrame().Count > 1)
            {
                headNote = InstantiateNoteObject(noteObject, rail, -1, true);
                StartCoroutine(LaunchNoteStream(notes.getCurrentLaunchTimeFrame(), holdTailNoteObj, rail, headNote.GetComponent<PathCreatorNoteMover>().noteId));
            }

            else
            {
                InstantiateNoteObject(noteObject, rail, -1, false);
            }

            notes.stageNext();
        }

        else
        {
            if (notes.getCurrentLaunchTimeFrame()[0] < currentTime)
            {
                notes.stageNext();
            }
        }
    }

    IEnumerator LaunchNoteStream(List<float> timeFrame, Transform noteObject, PathCreator rail, int parentId)
    {
        currentHoldNoteParentIds.Add(parentId);

        // Debug.Log("starting coroutine");
        float endTime = timeFrame[timeFrame.Count - 1];

        yield return new WaitForSeconds(GetComponentInParent<GameManager>().tailNoteSpacing);

        while (endTime > currentTime && currentHoldNoteParentIds.Contains(parentId))
        {
            TapSuccess4 tapSuccess4 = rail.transform.parent.GetComponentInChildren<TapSuccess4>();
            Transform tailNote = InstantiateNoteObject(noteObject, rail, parentId, false);
            if (tapSuccess4.currentHoldHeadId == parentId && !tapSuccess4.validHoldPeriod)
            {
                tailNote.gameObject.GetComponent<ScoreSuccess>().scorable = false;
                // tailNote.gameObject.GetComponent<ParticleSystem>().Stop();
            }

            yield return new WaitForSeconds(GetComponentInParent<GameManager>().tailNoteSpacing);
        }

        currentHoldNoteParentIds.Remove(parentId);
        // Debug.Log("coroutine ended");
    }

    Transform InstantiateNoteObject(Transform noteObject, PathCreator rail, int parentId, bool holdObject)
    {
        
        Transform note = Instantiate(noteObject, new Vector3(0.0f, 100.0f, 1000.0f), Quaternion.identity);
        note.GetComponent<PathCreatorNoteMover>().pathCreator = rail;
        note.GetComponent<PathCreatorNoteMover>().noteId = noteId;
        note.GetComponent<PathCreatorNoteMover>().parentId = parentId;
        note.GetComponent<PathCreatorNoteMover>().holdObject = holdObject;
        noteId++;

        note.SetParent(rail.transform);

        // Debug.Log(noteObject.name + " launch time: " + currentTime);
        // Debug.Log(noteObject.name + " parent id: " + note.GetComponent<PathCreatorNoteMover>().parentId);
        return note;
    }
}
