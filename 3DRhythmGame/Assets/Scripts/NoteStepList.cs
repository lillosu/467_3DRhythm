﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NoteStepList
{
    public List<NoteStep> noteSteps = new List<NoteStep>();
    private int currentNoteStepIdx = 0;

    public NoteStep getCurrentNoteStep()
    {
        return noteSteps[currentNoteStepIdx];
    }

    public void stageNextNoteStep()
    {
        currentNoteStepIdx++;
    }

}
