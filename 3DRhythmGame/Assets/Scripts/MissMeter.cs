﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MissMeter : MonoBehaviour
{
    public Slider slider;
    public float maxMisses;
    public static bool overMiss = false;
    // Start is called before the first frame update
    void Start()
    {
        overMiss = false;
        maxMisses = UnityEngine.Mathf.Floor(GameManager.noteCount * .3f);
        print("maxMisses " + maxMisses);
        slider.maxValue = maxMisses;
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = Notes_Spawn.missTotal;
        if (Notes_Spawn.missTotal >= maxMisses)
        {
            overMiss = true;
        }

    }

}
