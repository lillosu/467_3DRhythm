﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SongList
{
  public static AudioClip audioClip;
  static string[] externalFiles;
  static string activePath = "";
  static string winPath = "C:\\Users\\Public\\Music\\";
  static string songListPath = Path.Combine(Application.dataPath, "Resources", "Text", "SongList.txt");
  static string songListAssetPath = Path.Combine("Text", "SongList");

  public static List<string> GetSongs() {
    UnityEngine.Object[] audioClips;
    Scene currentScene = SceneManager.GetActiveScene();
    List<string> songList = new List<string>();

    if (currentScene.name == "SelectASong") {
      TextAsset songListAsset = Resources.Load<TextAsset>(songListAssetPath);
      if (songListAsset) {
        string[] lines = songListAsset.text.Split(';');
        foreach (string line in lines) {
          songList.Add(line);
        }
      }
      else {
        audioClips = Resources.LoadAll("Audio", typeof(AudioClip));
        foreach (var audioClip in audioClips)
          songList.Add(audioClip.name);

        Resources.UnloadUnusedAssets();
#if UNITY_EDITOR
        // Write songlist to text asset so we can won't have to load all resources. In practice this list won't change so we can save time building the scene.
        using (TextWriter tw = new StreamWriter(songListPath))
        {
          foreach (string song in songList)
            tw.Write(song + ";");
        }
#endif
      }

    } else {
        songList = GetUserSongs();
    }
    
      return songList;
    }

  public static List<string> GetUserSongs() {
    #if UNITY_EDITOR
      activePath = winPath;
#else
      activePath = GetAndroidMusicDirectory();
#endif

    var fileType = "*.mp3";
    externalFiles = Directory.GetFiles(activePath, fileType);

    List<string> userSongs = new List<string>();
    foreach (string songPath in externalFiles) {
      userSongs.Add(Path.GetFileNameWithoutExtension(songPath));
    }
      
    return userSongs;
  }

  public static void GetUserAudioClip(string songName) {
    #if UNITY_EDITOR
      activePath = winPath;
#else
      activePath = GetAndroidMusicDirectory();
#endif

    AudioSource source;
    
    WWW www = new WWW("file://" + activePath + songName + ".mp3");
    System.Threading.Thread.Sleep(2000);
    audioClip = www.GetAudioClip(false, false);
  }

  static string GetAndroidMusicDirectory() {
    List<string> paths = new List<string> {
      "/storage/emulated/0/Music/"
      , "/storage/extSdCard/Music/"
      };
      
    foreach (string path in paths) {
      if (Directory.Exists(path))
        return path;
    }

    return "";
  }
}

