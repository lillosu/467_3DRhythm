﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class PathCreatorNoteMover : MonoBehaviour
{
    public PathCreator pathCreator;
    float unitsPerSecond = 11.0f;
    public float dstTravelled;
    public int noteId = -1;
    public int parentId = -1;
    public bool holdObject = false;

    void Update()
    {
        
        dstTravelled += unitsPerSecond * Time.deltaTime;
        transform.position = pathCreator.path.GetPointAtDistance(dstTravelled);
        transform.rotation = pathCreator.path.GetRotationAtDistance(dstTravelled);

        foreach (PathCreatorNoteMover note in transform.parent.GetComponentsInChildren<PathCreatorNoteMover>())
        {
            if (noteId < note.noteId && transform.position.z > note.transform.position.z)
            {
                Destroy(gameObject);
            }
        }

        // this is a hack, we shouldn't be checking this every frame
        if (parentId > 0)
        {
            PathCreatorNoteMover parentNote = findParent();

            if (parentNote != null)
            {
                if ((Mathf.Abs(transform.position.z - parentNote.transform.position.z) < 0.5))
                {
                    Destroy(gameObject);
                }
            }
        }
    }

    
    PathCreatorNoteMover findParent()
    {
        PathCreatorNoteMover parentNote = null;
        foreach (PathCreatorNoteMover note in transform.parent.GetComponentsInChildren<PathCreatorNoteMover>())
        {
            if (note.noteId == parentId)
            {
                parentNote = note;
            }
        }

        return parentNote;
    }
}
