﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateSongList : MonoBehaviour
{

  [SerializeField]
  private Transform songPos;
  [SerializeField]
  private GameObject item;
  [SerializeField]
  private RectTransform content;

  public string[] songNames;

  // Start is called before the first frame update
  void Start()
  {
    var songNames = SongList.GetSongs();
    int i = 0;
    foreach (string song in songNames) {
      // Set selection to first song
      if (i == 0)
        LevelData.SongName = song;

      float posY = i * 20;
      Vector3 pos = new Vector3(0, -posY, songPos.position.z);
          
      GameObject songEntry = Instantiate(item, pos, songPos.rotation);

      songEntry.transform.SetParent(songPos, false);

      SongListDetail songListDetail = songEntry.GetComponent<SongListDetail>();

      songListDetail.text.text = song;
      ++i;
    }
  }

  // Update is called once per frame
  void Update()
  {
        
  }
}
