﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Reference: https://www.youtube.com/watch?v=fvdRKS8x0aM&t=894s
public class PathFollow : MonoBehaviour
{
    public Transform[] ObjectPath;
    public Transform newTerrain;
    public int currDest = 0;
    bool spawnTerrain = true;
    public static float speed = 50;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(ObjectPath[currDest].position, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, ObjectPath[currDest].position, Time.deltaTime*speed);

        if (dist <= 3000 && spawnTerrain)
        {
            Instantiate(newTerrain, new Vector3(-100, -10f, 2980), newTerrain.rotation);
            spawnTerrain = false;
        }
        if(dist <= 0)
        {
            Destroy(gameObject);
        }
        /*outdated code
        Vector3 direction =  ObjectPath[currLoc].position - transform.position;
        transform.position += direction * Time.deltaTime/speed;

        //allows for speed to change based on how much time has gone by
        //Currently is a mix of trying to not run out of level but not halt
        //currTime += Time.deltaTime;
        if (currTime > timeTracker)
        {
            timeTracker += 50;
            speed = speed/1.25f;
        }
        */
    }
}
