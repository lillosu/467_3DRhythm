﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Succ_Red : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

    //Mouse and touch controls for clicking on specified color tapper.
    //Has the success box move within range of tapper to check if note is
    //present within the window of time for success.
    void Update()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.name == "Red_Tapper")
                {
                    print("hit red_tapper"); 
                    GetComponent<Rigidbody>().velocity = new Vector3(0, .75f, 0);
                    StartCoroutine(retractCollider());
                   
                }
            }
        }
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.name == "Red_Tapper")
                {
                    print("hit red_tapper");
                    GetComponent<Rigidbody>().velocity = new Vector3(0, .75f, 0);
                    StartCoroutine(retractCollider());
                  
                }
            }
        }
#endif
    }

    //Retracts the specified collider back to 0 velocity
    IEnumerator retractCollider()
    {
        yield return new WaitForSeconds(.5f);
        GetComponent<Rigidbody>().velocity = new Vector3(0, -.75f, 0);
        yield return new WaitForSeconds(.5f);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);


    }
}
