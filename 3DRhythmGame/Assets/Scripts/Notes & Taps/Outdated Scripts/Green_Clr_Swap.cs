﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Green_Clr_Swap : MonoBehaviour
{
    MeshRenderer renderer;
    [Range(0, 1)] public float lerpTime;
    public Color color1;
    public Color color2;
    bool swap = false;
    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<MeshRenderer>();
    }

    //Mouse and touch controls for clicking on specified color tapper.
    //Has the success box move within range of tapper to check if note is
    //present within the window of time for success.
    void Update()
    {
        if (swap)
        {
            StartCoroutine(colorSwap());
        }

        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.name == "Green_Tapper")
                {
                    print("green tapper color change");
                    swap = true;
                }
            }
        }
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.name == "Green_Tapper")
                {
                    print("green tapper color change");
                    swap = true;
                }
            }
        }
#endif
    }
    IEnumerator colorSwap()
    {
        renderer.material.color = Color.Lerp(renderer.material.color, color1, 10 * lerpTime * Time.deltaTime);
        yield return new WaitForSeconds(.3f);
        renderer.material.color = Color.Lerp(color2, renderer.material.color, lerpTime * Time.deltaTime);

        swap = false;

    }
}
