﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blue_Check : MonoBehaviour
{
    // public Transform burst_fail;
    public Transform burst_blue;
    public Transform burst_fail;
    bool success = false;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    //If the note object collides with fail collider box, note is destroyed
    //Otherwise success collision with specified color box.
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Success_Blue")
        {
            Destroy(gameObject);
            Instantiate(burst_blue, transform.position, burst_blue.rotation);
            success = true;
            StartCoroutine(scoring(success));
            print("Success Blue, Streak " + Notes_Spawn.noteStreak);
            print("Current Score " + Notes_Spawn.totalScore);
        }
        if (other.gameObject.name == "FailCollider" && success == false)
        {
            Instantiate(burst_fail, transform.position, burst_fail.rotation);
            Destroy(gameObject);

            print("Fail");
            StartCoroutine(scoring(success));


        }
        else if (other.gameObject.name == "FailCollider" && success == true)
        {
            success = false;
        }


    }
    //Scoring System
    IEnumerator scoring(bool noteHit)
    {
        if (noteHit)
        {
            Notes_Spawn.noteStreak++;

            //10 successive notes hit increase hot-streak multiplier and can't be max multiplier
            if ((Notes_Spawn.hotStreak * 10) / Notes_Spawn.noteStreak <= 1 && Notes_Spawn.hotStreak != 16)
            {
                Notes_Spawn.hotStreak = Notes_Spawn.hotStreak * 2; //hot-streak gives x2,x4,x8,x16 multipliers.
            }

            Notes_Spawn.totalScore += 10 * Notes_Spawn.hotStreak;
        }
        else
        {
            Notes_Spawn.noteStreak = 0;
            print("missed note, streak reset");
            /* Miss note hot-streak goes down a level
            if(Notes_Spawn.hotStreak != 1)
            {
                Notes_Spawn.hotStreak = Notes_Spawn.hotStreak / 2;
            }
            */
        }
        yield return 0;
    }
}

