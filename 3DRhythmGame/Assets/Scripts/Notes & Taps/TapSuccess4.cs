﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//References: https://www.youtube.com/watch?v=kyp3Ks5a6to&t=1s
//https://www.youtube.com/watch?v=HYg7dsAjVMU&t=91s
//https://www.youtube.com/watch?v=aTtheFyh7Ac&t=346s
public class TapSuccess4 : MonoBehaviour
{

    [SerializeField] GameObject tapper;
    public Transform burst_input;
    public string input;

    //For Color Swap
    MeshRenderer renderer;
    [Range(0, 1)] public float lerpTime;
    public Color color1;
    public Color color2;
    bool swap = false;

    [SerializeField] int triggerkey;
    [SerializeField] Vector3 engagedLocation;
    [SerializeField] Vector3 retractedLocation;
    [SerializeField] float retractTime;

    //To prevent input spamming.
    public bool validHoldPeriod;
    public int currentHoldHeadId;
    const int holdFrameThreshold = 5; 

    NoteGenerator noteGenerator;

    private bool colliderEngaged;
    private int framesHeld;
    private List<Touch> currentTouches;
    private bool anyTouchHitObj;

    RuntimePlatform runtimePlatform;

    void Start()
    {
        renderer = tapper.GetComponent<MeshRenderer>();
        validHoldPeriod = false;
        currentHoldHeadId = -1;
        framesHeld = -1;
        noteGenerator = null;
        currentTouches = new List<Touch>();
        anyTouchHitObj = false;
        colliderEngaged = false;
        runtimePlatform = Application.platform;
    }

    //Mouse and touch controls for clicking on specified color tapper.
    //Has the success box move within range of tapper to check if note is
    //present within the window of time for success.
    void Update()
    {
        // in the case that the whole hold tail is successfully held, retract collider    
        if (!currentHoldTailsStillOnTrack())
        {
            validHoldPeriod = false;
        }

        // necessary because we delay the instantiation of the DataManger object
        if (noteGenerator == null)
        {
            if (GameObject.FindWithTag("DataManager"))
            {
                noteGenerator = GameObject.FindWithTag("DataManager").GetComponent<NoteGenerator>();
            }

        }

        //Starts Coroutine to swap color of the tapper
        if (PauseMenu.PausedGame == false)
        {
            if (swap)
            {
                StartCoroutine(colorSwap());
            }

            /* Each input follows the same logic, check that the input has collided with the correct tapper,
            check that the input is not locked from the last input, move the success collider within range of the success window
            do coroutine to retract collider to original position. */

            /******************* touch input *************************/
            if (runtimePlatform == RuntimePlatform.Android)
            {
                anyTouchHitObj = false;

                if (Input.touchCount > 0)
                {
                    currentTouches.Clear();
                    foreach (Touch touch in Input.touches)
                    {
                        Ray ray = Camera.main.ScreenPointToRay(touch.position);
                        RaycastHit hit;
                        if (Physics.Raycast(ray, out hit))
                        {
                            // Debug.Log("hit collider and tapper name: " + hit.collider.name + ", " + tapper.name);
                            if (hit.collider.name == tapper.name)
                            {
                                currentTouches.Add(touch);
                                anyTouchHitObj = true;
                            }
                        }
                    }

                    if (anyTouchHitObj)
                    {
                        if (hasPhase(currentTouches, TouchPhase.Ended))
                        {
                            // Debug.Log("touch end detected");
                            processTapRelease();
                        }

                        if (hasPhase(currentTouches, TouchPhase.Began))
                        {
                            Notes_Spawn.totalTaps++;
                            print("Taps "+Notes_Spawn.totalTaps);
                            engageCollider();
                        }

                        if (hasPhase(currentTouches, TouchPhase.Stationary) || hasPhase(currentTouches, TouchPhase.Moved))
                        {
                            assessHoldValidity();
                        }
                    }

                    else
                    {
                        if (validHoldPeriod)
                        {
                            earlyHoldReleaseBehavior();
                        }

                        hardResetState();
                    }
                }

                else
                {
                    hardResetState();
                }
            }

            /******************* keyboard input *************************/
            else
            {
                if (Input.GetKeyDown(triggerkey.ToString()))
                {
                    // Debug.Log(triggerkey.ToString() + " key was pressed");
                    Notes_Spawn.totalTaps++;
                    print("Taps " + Notes_Spawn.totalTaps);
                    engageCollider();
                }

                if (Input.GetKey(triggerkey.ToString()))
                {
                    assessHoldValidity();
                }

                if (Input.GetKeyUp(triggerkey.ToString()))
                {
                    // Debug.Log(triggerkey.ToString() + "key released");
                    processTapRelease();
                }

//#if UNITY_EDITOR
//                /******************* mouse click input *************************/
//                if (Input.GetMouseButton(0))
//                {
//                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
//                    RaycastHit hit;
//                    if (Physics.Raycast(ray, out hit))
//                    {
//                        if (hit.collider.name == tapper.name)
//                        {
//                            engageCollider();
//                        }
//                    }
//                }
//#endif
            }
        }

        else
        {
            retractCollider();
        }
    }

    private void hardResetState()
    {
        validHoldPeriod = false;
        framesHeld = 0;
        if (colliderEngaged)
        {
            retractCollider();
        }
    }

    private void processTapRelease()
    {
        framesHeld = 0;
        retractCollider();

        if (validHoldPeriod)
        {
            earlyHoldReleaseBehavior();
        }

        validHoldPeriod = false;
    }

    private void earlyHoldReleaseBehavior()
    {
        foreach (PathCreatorNoteMover note in gameObject.transform.parent.GetComponentsInChildren<PathCreatorNoteMover>())
        {
            noteGenerator.currentHoldNoteParentIds.Remove(currentHoldHeadId);
            if (note.parentId == currentHoldHeadId)
            {
                note.gameObject.GetComponent<ScoreSuccess>().initiateFailBurstAtNote();
                note.gameObject.GetComponent<ScoreSuccess>().scorable = false;
                note.gameObject.GetComponent<ParticleSystem>().Stop();
                //Destroy(note.gameObject);
            }
        }
    }

    private void assessHoldValidity()
    {
        framesHeld++;
        if (framesHeld > holdFrameThreshold)
        {
            if (!validHoldPeriod)
            {
                framesHeld = 0;
                retractCollider();
            }
        }
    }

    private void engageCollider()
    {
        if (!colliderEngaged)
        {
            framesHeld = 1;
            colliderEngaged = true;
            transform.position = new Vector3(engagedLocation.x, engagedLocation.y, engagedLocation.z);
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

            // print("tapper color change");
            swap = true;
        }
    }

    //Retracts the specified collider back to 0 velocity
    void retractCollider()
    {
        // waits fraction of a second to allow collider to hit note,
        //then places back in original position with no velocity.
        // yield return new WaitForSeconds(retractTime);
        transform.position = new Vector3(retractedLocation.x, retractedLocation.y, retractedLocation.z);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        colliderEngaged = false;
    }

    IEnumerator colorSwap()
    {
        //Uses lerp to swap colors
        renderer.material.color = Color.Lerp(renderer.material.color, color1, 10 * lerpTime * Time.deltaTime);
        Instantiate(burst_input, tapper.transform.position, burst_input.rotation);

        yield return new WaitForSeconds(retractTime);
        renderer.material.color = Color.Lerp(color2, renderer.material.color, lerpTime * Time.deltaTime);

        swap = false;
    }

    bool hasPhase(List<Touch> touches, TouchPhase phase)
    {
        foreach (Touch touch in touches)
        {
            if (touch.phase == phase)
            {
                return true;
            }
        }

        return false;
    }

    bool currentHoldTailsStillOnTrack()
    {
        foreach (PathCreatorNoteMover note in gameObject.transform.parent.GetComponentsInChildren<PathCreatorNoteMover>())
        {
            if (note.parentId == currentHoldHeadId)
            {
                return true;
            }
        }

        return false;
    }
}