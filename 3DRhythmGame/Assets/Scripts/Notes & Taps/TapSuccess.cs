﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//References: https://www.youtube.com/watch?v=kyp3Ks5a6to&t=1s
//https://www.youtube.com/watch?v=HYg7dsAjVMU&t=91s
//https://www.youtube.com/watch?v=aTtheFyh7Ac&t=346s
public class TapSuccess : MonoBehaviour
{

    [SerializeField] GameObject tapper;
    public string input;

    //For Color Swap
    MeshRenderer renderer;
    [Range(0, 1)] public float lerpTime;
    public Color color1;
    public Color color2;
    bool swap = false;

    //To prevent input spamming.
    bool lockInput;

    // Start is called before the first frame update
    void Start()
    {
        //Assigns which success colider is in use.
        lockInput = false;
        renderer = tapper.GetComponent<MeshRenderer>();
        if (transform.gameObject.name == "Success_Red")
        {
            input = "1";
        }
        if (transform.gameObject.name == "Success_Blue")
        {
            input = "2";
        }
        if (transform.gameObject.name == "Success_Green")
        {
            input = "3";
        }

    }

    //Mouse and touch controls for clicking on specified color tapper.
    //Has the success box move within range of tapper to check if note is
    //present within the window of time for success.
    //Now has keyboard input for 1,2,3.
    void Update()
    {

        //Starts Coroutine to swap color of the tapper
        if (swap)
        {
            StartCoroutine(colorSwap());
        }
        //Each input follows the same logic, check that the input has collided with the correct tapper,
        //check that the input is not locked from the last input, move the success collider within range of the success window
        //do coroutine to retract collider to original position.
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.name == tapper.name && lockInput == false)
                {
                    print("hit " + tapper.name);
                    GetComponent<Rigidbody>().velocity = new Vector3(0, 0, -3);
                    StartCoroutine(retractCollider());

                    print("tapper color change");
                    swap = true;
                }
            }
        }
        if (Input.GetKeyDown(input) && lockInput == false)
        {
            lockInput = true;
            print(input + " key was pressed");
            if (input == "1")
            {
                transform.position = new Vector3(-1.886532f, -.02f, -3.26f);
                GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            }
            if (input == "2")
            {
                transform.position = new Vector3(-0.5865319f, -.02f, -3.26f);
                GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            }
            if (input == "3")
            {
                transform.position = new Vector3(0.7134681f, -.02f, -3.26f);
                GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            }



            print("tapper color change");
            swap = true;

        }
        if (Input.GetKeyUp(input) && lockInput)
        {

            StartCoroutine(retractCollider());
        }
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.name == tapper.name && lockInput == false)
                {
                    lockInput = true;
                    print("hit " + tapper.name);
                    GetComponent<Rigidbody>().velocity = new Vector3(0, 0, -3f);
                    StartCoroutine(retractCollider());

                    print("tapper color change");
                    swap = true;

                }
            }
        }



#endif
}


    //Retracts the specified collider back to 0 velocity
    IEnumerator retractCollider()
    {
        if (input == "1")
        {
            transform.position = new Vector3(-1.886532f, -.02f, -1.89f);
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
        if (input == "2")
        {
            transform.position = new Vector3(-0.5865319f, -.02f, -1.89f);
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
        if (input == "3")
        {
            transform.position = new Vector3(0.7134681f, -.02f, -1.89f);
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }

        yield return new WaitForSeconds(.3f);
        lockInput = false;

    }
    IEnumerator colorSwap()
    {
        //Uses lerp to swap colors
        renderer.material.color = Color.Lerp(renderer.material.color, color1, 10 * lerpTime * Time.deltaTime);
        yield return new WaitForSeconds(.3f);
        renderer.material.color = Color.Lerp(color2, renderer.material.color, lerpTime * Time.deltaTime);

        swap = false;

    }
}