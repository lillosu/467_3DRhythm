﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapperAnimation : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0f, PathFollow.speed/2f, 0f) * Time.deltaTime);
    }
}
