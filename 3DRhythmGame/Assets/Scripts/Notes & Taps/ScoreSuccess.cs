﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScoreSuccess : MonoBehaviour
{
    [SerializeField] GameObject[] colliders;
    [SerializeField] int pointValue;
    public GameObject purpStrike;
    List<string> colliderNames;
    public Transform burst_succ;
    public Transform burst_fail;
    public int colliderIdx;
    private Transform rail;
    private GameObject tapper;
    private TapSuccess4 tapSuccess4;
    private PathCreatorNoteMover thisNote;
    NoteGenerator noteGenerator;
    public Transform burst_mult;
    public Transform burst_strike;
    public Transform burst_speed;

    public bool scorable;
    bool success = false;

    // Start is called before the first frame update
    void Start()
    {
        scorable = true;
        rail = gameObject.transform.parent;
        tapSuccess4 = rail.parent.GetComponentInChildren<TapSuccess4>();
        thisNote = transform.GetComponent<PathCreatorNoteMover>();
        noteGenerator = GameObject.FindWithTag("DataManager").GetComponent<NoteGenerator>();
        colliderNames = new List<string>();

        for (int i = 0; i < colliders.Length; i++)
        {
            colliderNames.Add(colliders[i].transform.name);
        }

        Debug.Log(colliderNames.Count);

        tapper = getTapperFromRail();
    }

    // Update is called once per frame
    void Update()
    {
    }
    //If the note object collides with fail collider box, note is destroyed
    //Otherwise success collision with specified color box.
    void OnTriggerEnter(Collider other)
    {
        if (colliderNames.Contains(other.gameObject.name))
        {
            if (scorable)
            {
                // allow holding
                if (thisNote.holdObject == true)
                {
                    tapSuccess4.validHoldPeriod = true;
                    tapSuccess4.currentHoldHeadId = thisNote.noteId;
                }

                //Perform particle effect
                Instantiate(burst_succ, tapper.transform.position, burst_succ.rotation);

                Destroy(gameObject);
                success = true;//Prevents edge case of registering case of failcollider colision.

                if (thisNote.parentId < 0)
                {
                    Notes_Spawn.totalSucc++;
                    print("total succ" + Notes_Spawn.totalSucc);
                    scoring(success);//Modify scoring
                }

                else
                {
                    Notes_Spawn.totalScore = Notes_Spawn.totalScore + (Notes_Spawn.hotStreak * pointValue);
                }

                print("Success, Streak " + Notes_Spawn.noteStreak);
                print("Current Score " + Notes_Spawn.totalScore);
            }
        }
        else if (other.gameObject.name == "FailCollider" && success == false)
        {
            //Release fail particles at failcollider
            if (scorable)
            {
                initiateFailBurstAtTapper();
            }

            // allow holding
            if (thisNote.holdObject == true)
            {
                tapSuccess4.validHoldPeriod = false;
                foreach(Transform child in rail)
                {
                    Debug.Log("child name: " + transform.name);
                    if (child.GetComponent<PathCreatorNoteMover>().parentId == thisNote.noteId)
                    {
                        child.GetComponent<ScoreSuccess>().initiateFailBurstAtNote();
                        noteGenerator.currentHoldNoteParentIds.Remove(thisNote.noteId);

                        // process failure
                        // Destroy(child.gameObject);
                        child.GetComponent<ScoreSuccess>().scorable = false;
                        child.GetComponent<ParticleSystem>().Stop();
                    }
                }
            }

            Destroy(gameObject);
            print("Fail");

            if (thisNote.parentId < 0)
            {
                scoring(success); // modify scoring coroutine
            } 
        }
        if (other.gameObject.name == "FailCollider" && success == true)
        {
            success = false;
        }
        

    }
    //Scoring System
    void scoring(bool noteHit)
    {
        if (noteHit)
        {

            if (Notes_Spawn.noteStreak > 0 && Notes_Spawn.hotMeter < 10)
            {
                    Notes_Spawn.hotMeter++;
            }
            //10 build-up increase hot-streak multiplier and can't be max multiplier (for testing)
            if (Notes_Spawn.hotMeter >= 10 && Notes_Spawn.hotStreak !=16)
            {
                Instantiate(burst_mult, new Vector3(2.43f, 11.57f, 0), burst_mult.rotation);
                Instantiate(burst_speed, new Vector3(0, 5.28f, 0), burst_speed.rotation);
                Notes_Spawn.hotStreak = Notes_Spawn.hotStreak * 2; //hot-streak gives x2,x4,x8,x16 multipliers.
                PathFollow.speed = PathFollow.speed * 1.5f;
                Notes_Spawn.hotMeter = 0;//Reset Meter for next intensity
            }
            Notes_Spawn.noteStreak++;
            Notes_Spawn.totalScore = Notes_Spawn.totalScore + (Notes_Spawn.hotStreak * pointValue);

            //Remove miss-streak icons once note-streak has begun

            if (Notes_Spawn.missStreak > 0) {
                Destroy(Notes_Spawn.strike1);
                Destroy(Notes_Spawn.strike2);
                Destroy(Notes_Spawn.strike3);

            }
            Notes_Spawn.missStreak = 0;
        }      
        else
        {
            Notes_Spawn.noteStreak = 0;           
            Notes_Spawn.missTotal++;
            Notes_Spawn.missStreak++;
            if (Notes_Spawn.missStreak == 1)
            {
                Notes_Spawn.strike1 = Instantiate(purpStrike, new Vector3(-1.25f, 10.7f, .35f), transform.rotation);
            }
            if (Notes_Spawn.missStreak == 2)
            {
                Notes_Spawn.strike2 = Instantiate(purpStrike, new Vector3(-.65f, 10.7f, .35f), transform.rotation);
            }
            if (Notes_Spawn.missStreak == 3)
            {
                Notes_Spawn.strike3 = Instantiate(purpStrike, new Vector3(-.5f, 10.7f, .35f), transform.rotation);
            }

            print("missed note, streak reset");
            if (Notes_Spawn.missStreak >= 3)
            {

                if (Notes_Spawn.hotStreak != 1)//if not default hot streak
                {
                        Notes_Spawn.hotStreak = Notes_Spawn.hotStreak / 2;
                        Notes_Spawn.hotMeter = 5;
                        PathFollow.speed = PathFollow.speed / 1.5f;
                }
                else
                {
                    Notes_Spawn.hotMeter = 0;
                }
                /*
                if (PathFollow.speed > 50)
                {
                    PathFollow.speed = PathFollow.speed / 1.5f;
                }
                */
                Notes_Spawn.missStreak = 0;
                Destroy(Notes_Spawn.strike1);
                Destroy(Notes_Spawn.strike2);
                Destroy(Notes_Spawn.strike3, .05f);
                Instantiate(burst_strike, new Vector3(-.5f, 10.7f, .35f), burst_strike.rotation);

            }
            else
            {
                Notes_Spawn.hotMeter = 0;
            }

        }

        return;
    }

    public void initiateFailBurstAtNote()
    {
        Instantiate(burst_fail, transform.position, burst_fail.rotation);
    }
    public void initiateFailBurstAtTapper()
    {
        Instantiate(burst_fail, tapper.transform.position, burst_fail.rotation);
    }

    private GameObject getTapperFromRail()
    {
        string railName = rail.gameObject.name;
        string tapperColor = railName.Substring(0, railName.IndexOf("Rail"));
        return GameObject.Find(tapperColor + "_Tapper");
    }

}
