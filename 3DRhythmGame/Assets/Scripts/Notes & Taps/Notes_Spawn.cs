﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notes_Spawn : MonoBehaviour
{
    //Predetermined order to spawn notes (1 red, 2 blue, 3 green)
    List<float> eachNote = new List<float>() { 1, 1, 2, 3, 3, 2, 1, 3, 2, 2, 2, 1 };
    public int notePos = 0; //Current position in list

    //Scoring Sytem variables throughout system
    public static int noteStreak = 0;//track note-streak
    public static int hotStreak = 1;//track hot-streak
    public static int totalScore = 0;//track total score
    public static int missStreak = 0;//track miss streak
    public static int missTotal = 0;//track miss total
    public static int hotMeter = 0;//track miss streak
    public static float totalTaps = 1f;//track total taps
    public static float totalSucc = 1f;//track total successes
    public static float accuracy = 1f;//track total successes

    public static GameObject strike1;
    public static GameObject strike2;
    public static GameObject strike3;

    //Note Objects to spawn based on color
    public Transform noteObj_Red;
    public Transform noteObj_Green;
    public Transform noteObj_Blue;
    public Transform noteObj;

    public string timeReset = "y"; //To check if enough time has gone by to spawn next note.
    public float xPos; //Track position note should spawn at.

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(timeReset == "y") //Yes enough time has gone by to spawn
        {
            StartCoroutine(spawnNote());
            timeReset = "n";
        }
    }

    //Wait a second then spawn note, update position and object based on idx of list
    IEnumerator spawnNote()
    {
        yield return new WaitForSeconds(1);

        if(eachNote[notePos] == 1)
        {
            xPos = -1.3f;
            noteObj = noteObj_Red;
        }
        if (eachNote[notePos] == 2)
        {
            xPos = 0f;
            noteObj = noteObj_Blue;
        }
        if (eachNote[notePos] == 3)
        {
            xPos = 1.3f;
            noteObj = noteObj_Green;
        }

        notePos++;
        timeReset = "y";
        Instantiate(noteObj, new Vector3(xPos, 10f, -5.5f), noteObj.rotation);
    }
}
