﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSong : MonoBehaviour
{
    [SerializeField] string songName;
    [SerializeField] string noteStepJsonName;

    public void setSong()
    {
        LevelData.SongName = songName;
        LevelData.NoteJson = noteStepJsonName;
    }
}
