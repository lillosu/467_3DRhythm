﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetDifficulty : MonoBehaviour
{
    public void setDifficulty()
    {
        if (gameObject.GetComponent<Slider>().value == 0)
        {
            LevelData.hardMode = false;
        }

        else
        {
            LevelData.hardMode = true;
        }
    }
}
